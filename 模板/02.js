//注意：带有双斜杠内容为注释，不要动，英语括号内只能写英语，遵守驼峰命名法！
//import 是导依赖用的，根据提示直接写就可以，不用管，此bot为Javascript所写，其他语言请自己解析 
//JavaScript编程建议使用VSC，Java建议使用IDEA，两种语言基本使用方法可以参考目录
import { segment } from "icqq";
import plugin from '../../lib/plugins/plugin.js'
import axios from 'axios';

export class <类名称> extends plugin {
    constructor() {
        super(
            {
                name: '插件名',
                desc: '提示信息',
                event: 'message',//发出提示信息
                priority: '50',//优先级
            }
        )
    }
}